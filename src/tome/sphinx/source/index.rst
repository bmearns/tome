.. Tome documentation master file, created by
   sphinx-quickstart on Thu Jan 17 09:40:11 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Tome's documentation!
================================

Contents:

.. toctree::
   :maxdepth: 2

   tome
   writers


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

