{#  "

    This file is the "Package Document" which is the primary source of
    information about the publication. In addition to meta data such as the
    title, author, copyright, etc., it also contains important structural
    information for the epub reader, such as the manifest which describes
    all of the files contained in the epub.

    Edit this file to add your own information specific to you and your
    book. This file will actually serve as a template for generating the
    actual OPF file using the templ processor. Basically, anything wrapped
    in curly braces will be processed in some way, usually either stripped
    away or replaced with content from your Tome object.

    For more information on contents of this document, see
    <http://idpf.org/epub/30/spec/epub30-publications.html#sec-package-documents>

    "
}{v{:

}}{syntax xml

}<?xml version="1.0"?>
<package
    version="3.0"
    xmlns="http://www.idpf.org/2007/opf"
    unique-identifier="BookId"
>
    <!-- META {#
        "

        The metadata section contains meta data for your book, such as
        the title, the authors, copyright information, etc.

        Most of the tags in this section should be from the Dublin Core
        Metadata Element Set (DCMES, or simply DC), using the 'dc:'
        prefix. You can also use 'meta' tags, and 'link' tags.

        There are three required tags here: 'dc:title', 'dc:identifier',
        and 'dc:language' The title is simply the main (primary) title
        of the book. The identifier is a unique identifier that uniquely
        specifies this book, for instance an ISBN number, or a URI which
        you can ensure will be unique. The language specifies the language
        the book is written in, conforming to RFC5646 (e.g., 'en-US'
        for United States English).

        For more information on the metadata element, see
        <http://idpf.org/epub/30/spec/epub30-publications.html#sec-metadata-elem>

        For RFC5646 (for the 'dc:language' tag), see
        <https://tools.ietf.org/html/rfc5646>

        "
    } -->
    <metadata
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:opf="http://www.idpf.org/2007/opf"
    >
        <!-- Three required fields: title, identifier, and language. -->
        <dc:title id='title'>{$ TITLE}</dc:title>
            <meta refines="#title" property="title-type">main</meta>
            <meta refines="#title" property="display-seq">1</meta>
        <dc:identifier id="BookId">{$ UID}</dc:identifier>
        <dc:language>{$ LANG}</dc:language> 
        <!-- (end of required fields) -->

        <!-- For legacy, specify the title using the old meta tag. -->
        <meta name="DC.Title.Main" content="{$ TITLE}" />

        <!-- Specify all the subtitles. -->
        {$ SUBTITLES-DC-XML}

        <!-- Author(s) of the book -->
        {$ AUTHORS-DC-XML}

        <!-- The date the book was last modified. -->
        <meta property="dcterms:modified">{$ MODIFIED}</meta>


        <!-- Project specific meta data -->
        <!-- {#
            "

            You can add additional metadata elements here. Some common ones include 'dc:subject',
            'dc:description', 'dc:publisher', and 'dc:rights' (the last is a copyright statement).

            For more info, see
            <http://idpf.org/epub/30/spec/epub30-publications.html#sec-opf-dcmes-optional>,
            <http://idpf.org/epub/30/spec/epub30-publications.html#elemdef-meta>,
            <http://idpf.org/epub/30/spec/epub30-publications.html#sec-opf-meta-elem>, and
            <http://idpf.org/epub/30/spec/epub30-publications.html#elemdef-opf-link>

            For the complete set of available Dublin Core elements, see
            <http://dublincore.org/documents/dces/>

            "
        } -->

    </metadata>


    <!-- MANIFEST {#
        "
        
        The manifest section lists and describes every file included in the
        epub. Any files you add or remove must be included here.

        For more information, see
        <http://idpf.org/epub/30/spec/epub30-publications.html#sec-manifest-elem>

        "
    } -->
    <manifest>
        <!-- Nav -->
        <item id="nav" href="nav.xhtml" properties="nav" media-type="application/xhtml+xml" />
        <item id="ncxToc" href="toc.ncx" media-type="application/x-dtbncx+xml" />

        <!-- Misc. Resources -->
        <item id="style" href="stylesheet.css" media-type="text/css" />
        <item id="pagetemplate" href="page-template.xpgt" media-type="application/vnd.adobe-page-template+xml" />

        <!-- Front matter -->
        <item id="frontcover" href="frontcover.xhtml" media-type="application/xhtml+xml" />
        <item id="titlepage" href="title_page.xhtml" media-type="application/xhtml+xml" />
        <item id="toc" href="toc.xhtml" media-type="application/xhtml+xml" />

        <!-- Text content -->
        {$ MANIFEST-XML}

        <!-- Back matter -->
        <item id="endnotes" href="endnotes.xhtml" media-type="application/xhtml+xml" />
     </manifest>


    <!-- SPINE {#
        "
        
        The spine section specifies the default reading order of all content
        files in the epub, by referencing item id's from the manifest.
        Manifest items that are not included in the spine are generally
        considered ancillary to the primary content and are not part of the
        normal reading of the book.

        For more information, see 
        <http://idpf.org/epub/30/spec/epub30-publications.html#sec-spine-elem>

        "
    } -->
    <spine toc="ncxToc">
        <itemref idref="frontcover" />
        <itemref idref="titlepage" />
        <itemref idref="toc" />
        {$ SPINE-XML}

        <itemref idref="endnotes" />
    </spine>

    <!-- GUIDE {#
        "

        The guide section is deprecated in EPUB v3, but may be used by EPUB v2
        readers. The purpose is to identify common major structural elements
        of the work, such as a front cover, table of content, the first page
        of the 'real' content, etc.

        For more information, see
        <http://idpf.org/epub/30/spec/epub30-publications.html#sec-guide-elem>,
        and
        <http://idpf.org/epub/20/spec/OPF_2.0.1_draft.htm#Section2.6>

        "
    } -->
    <guide>
        <reference type="cover" title="Front Cover" href="frontcover.xhtml" />
        <reference type="title-page" title="Title Page" href="title_page.xhtml" />
        <reference type="toc" title="Table of Contents" href="toc.xhtml" />
        <reference type="text" title="Text" href="chapter0001.xhtml" />
        <reference type="notes" title="All Chapter Notes" href="endnotes.xhtml" />
    </guide>

</package>{
end-syntax xml
}
