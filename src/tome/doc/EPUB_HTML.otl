Classes and IDs used in generated HTML.
General Overview:
	:
	: Classes generally make more sense, given that they can be applied to
	: multiple elements within the same document. However, when there is very
	: reasonably at most one instance of an item, or when there is one
	: instance of an item which is obviously the most significant, then an ID
	: can be used. If there may be multiple and none and obviously more
	: significant, do not use an ID: a class will suffice whether there is one
	: or multiple.
	:

Structural Units
	Overview:
		:
		: Structural units refers to the major units within the work,
		: such as front cover, title page, table of contents, sections of
		: content, indices, appendices, afterwards, etc.
		:
		: Every structural unit should be encapsulated in a structural-
		: element in the HTML. If the unit is included in a larger context,
		: then the structural-element should be a DIV element. Otherwise, the
		: unit should be in an XHTML file of it's own, in which case it's
		: structural-element is the file's BODY element.
		:
		: For content, the smallest structural unit is a chapter: neither a
		: scene nor a paragraph are considered structural units.
		:
		-Reference: http://idpf.org/epub/vocab/structure/
	HTML Classes:
		Top Level:
			.cover
				-Front and back cover, spine, jacket, etc.
			.frontmatter
				-Applies to any units preceeding the "main" content of the work.
			.bodymatter
				-Applies to all of the "main" content units of the work, which would include
				parts, book, chapters, etc.
			.backmatter
				-Applies to any units following the "main" content of the work, such as indices (usually),
				appendices, afterwards, epilogues, etc.
		Front Matter:
			.epigraph
				-Typically a quote of relevance to the matter. This could be applied to each individual epigraph,
				or if they are grouped together, they can be encapsulated and the class applied to the encapsulation.
				See also #epigraph for a page containing all of the epigraphs.
			.introduction
				-A unit of content introducing the work, typically by the author. If not written by the author,
				it is usually a .foreword. There may be multiple introdcutions, typically from different editions.
				Introductions from earlier editions are often included.
			.foreword
				-A unit of content introducing the work, typically *not* written by the author. If written by
				the author, it is usually an .itroduction. There may be multiple forewords, from different people.
			.acknowledgments
				-A unit acknowledging parties that were involved in producing or otherwise bringing about the work.
			.dedication
				-A unit addressing particular people, typically unrelated to actual realization of the work.
				See also #dedication for a page containing all of the dedications.
			Tables of Contents:
				.toc
					-Applies to any table of contents.
				.landmarks
					-Applies to any TOC which references well-known sections of the document (e.g., "Preface,
					Introduction, Content, Epilogue, Appenidices, Index").
				#tocChapter{chNum}
					-Used for chapter entries, in the #toc only.
				#tocDetailedChapter{chNum}
					-Used for chapter entries, in the #tocDetailed only.

		Body Matter:
			.chapter
				-Must be applied to all chapters, chapters must be encapsulated. Also applied to chapter entries in
				TOCs.
			.scene
				-MAY be applied to all scenes, if the scenes are encapsulated. Also applied to scene entries in TOCs.
			.sceneSep
				-Must be applied if any element is used to separate scenes.
			.firstScene
				-If scenes are encapsulated and given the .scene class, then the first scene in each chapter
				MUST be given this class. If a scene separator is used, then .firstScene class MUST be applied
				to the scene separator for the first scene in each chapter.
			.lastScene
				-Like .firstScene, but for the last scene in a chapter.
			Reference material:
				.noteref
					-A reference to a footnote or endnote.
				.note
					-A footnote or endnote. Notes usually appear in a list, and this class applies to the entire list
					entry. Typically, the list of notes would be a dictionary list. A note should be linked to with
					a .noteref classed element.
				.rearnotes
					-A collection of endnotes, appearing either at the end of the chapter/section, or at the end of
					the document.
		Back Matter:
			.appendix
				-Applies to any appendix in the document.
			.index
				-Applies to any index in the document. An index is a list of terms/topics that appear in the document,
				and references to where they occur. There may be different indices, for instance, an index of people,
				an index of dates, and index of definitions, etc. If there is a general ("complete") index, then it should
				have the #index ID as well. No other index should, regardless of whether or not the general index exists.
			.glossary
				-Applies to any glossary, which lists and defines words. There could conceivably be multiple glossaries,
				for instance, a general glossary, a glossary of abbreviations, a glossary of fictional words, a glossary
				of places, a glossary of foreign words, etc. If there is a general glossary, whether there are other
				glossaries or not, then the general glossary should have the #glossary id as well. No other glossaries
				should be given this id, whether the general glossary exists or not.
	HTML IDs:
		Cover:
			#frontcover
				-The front cover of the document.
			#backcover
				-The back cover of the document.
			#spine
				-The spine of the document.
		Front Matter:
			#epigraph
				-If any and all epigraphs are collected together, then this would refer to the entire collection.
				E.g., the "epigraph page". If this is used, the .epigraph class should be applied to the same element
				and may not need to be applied to all of the epigraphs inside, depending on the content.
			#dedication
				-If any and all dedications are collected together, then this would refer to the entire collection.
				E.g., the "dedication page". If this is used, the .dedication class should be applied to the same element
				and may not need to be applied to all of the dedications inside, depending on the content.
			#acknowledgments
				-If any and all acknowledgements are collected together, then this would refer to the entire collection.
				E.g., the "acknowledgments page".
			#copyright-page
				-A unit describing the copyright of the work. May also include a *summary* of the license.
			#titlepage
				-The main title page. Note that this is typically distinct from the #frontcover.
			#preface
				-A unit of content typically written by the author which describes the "backstory" of the work, e.g., 
				what inspired the author to write it. There is typically only one preface for a work.
			#introduction
				-The primary instance of .introduction. This would likely be the introduction to the current edition.
			#prologue
				-An opening to a story, written in the context of the story, typically setting the scene or providing
				some historical context. Often applies earlier that the remaining content of the story and is often told
				from the narrator's perspective and in the narrator's voice.
		Back Matter:
			#index
				-The general ("complete") index, if it exists. See .index.
			#glossary
				-The general glossary if it exists. See .glossary.
			#bibliography
				-Applies to the bibliography.
		Reference material:
			#endnotes
				-Applies only to the document-wide collection of end notes, if there is one. Does NOT apply to the
				endnotes at the end of a section.
		Tables of Content:
			#toc
				-Applies to the structural unit representing the main table of contents, if there is one.
				The main table of contents is usually fairly compact, not going deeper than chapter level.
			#tocSummary
				-Applies to a summary table of contents, if present. This TOC should be a subset of
				(or equal to) the main toc (identified by #toc). This would typically only go as deep
				as the highest useful level for content, for instance part, if there is more than one part;
				or book if there is more than one book. It would not typically list each piece of front matter
				or backmatter individually, but may break it down into major sections, or simply reference
				each as a whole.
			#tocDetailed
				-Applies to a more details table of contents than the main TOC (as identified by the #toc id).
				If this exists, it will typically be a very thorough listing of content and go quite deeply
				into the heirarchy (e.g., scenes within a chapter, possibly down to the paragraph level). If
				present, it should be a superset of (or identical to) the main TOC. If present, it may be appropriate
				to include as backmatter.

Titles:
	Overview
		:
		: Applies to all sections, as well as the entire document.
		:
	HTML Classes
		.titleGeneric
			-Used for generic titles, like "Chapter 6" or "Book III".
		.title
			-Used for all titles, both work and chapter titles, main and subtitles. The context will
			be determined by the structural unit in which is occurs (e.g., chapter, titlepage, etc).
			It can be refined by the other title classes. For subtitles, also apply the .subtitle class.
			The main title for the unit is the one without any .subtitle class applied.
		.subtitle
			-Used for all subtitles. Up to 4 levels of subtitles can be explicitely specified using the
			.subtitle1, .subtitle2, .subtitle3, and .subtitle4, etc.
		.subtitle1, etc...
			-The first subtitle. .subtitle2, .subtitle3, etc., indefinitely.
		.lastTitle
			-The last title in a series of titles (e.g., a series of subtitles). An element which has the
			.title class and no .subtitle class is the first title. The element with class .subtitle1
			is the first sub title.

Authors:
	HTML Classes
		.author
			-Used for all authors of the document.

Locations:
	HTML IDs
		#chapter{chNum}
		#ch{chNum}Scene{scNum}
		#ch{chNum}Notes
			-Chapter end notes.
		#ch{chNum}N{enNum}
			-Chapter end notes, for the specified note.
		#ch{chNum}N{enNum}Ref
			-The reference to an end note
		#ch{chNum}N{enNum}Ref{refNum}
			-If there are multiple references to a given note, use ths for each reference. The most important
			or relevant reference (and only that one) should have #ch{chNum}N{enNum}Ref only, and the first
			refNum is 2.
		#endnotes
			-See above
		#endnotesCh{chNum}
			-Collected end notes, for the specified chapter.
		#endnotesCh{chNum}N{enNum}
			-Collected end notes, for the specified note of the specified chapter.


