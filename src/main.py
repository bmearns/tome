#!/usr/bin/python
"""
Copyright 2013 Brian Mearns

This file is part of Tome.

Tome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Tome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Tome.  If not, see <http://www.gnu.org/licenses/>.
"""
"""
This file is not part of the distribution, it is just for running the main function from src.
Trying to run tome.main directory causes there to be no tome module, so resource access fails.
"""

import tome.main
import sys

if __name__ == "__main__":
    ret = tome.main.main()
    if ret is None:
        ret = 0
    sys.exit(ret)
